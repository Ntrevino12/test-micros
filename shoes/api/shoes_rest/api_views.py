from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import Shoe, BinVO
from common.json import ModelEncoder

class BinVoEncoder(ModelEncoder):
    model = BinVO
    properties =[
        "closet_name",
        "import_href",
    ]

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "name",
        "color",
        "picture",
        "bin",
        "id",
    ]
    encoders = {
        "bin": BinVoEncoder(),
    }
class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "name",
        "id",
    ]
    def get_extra_data(self, o):
        return {"color": o.color,
                "picture": o.picture,
                "manufacturer": o.manufacturer,
                "bin": o.bin.closet_name,
                }



@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoe = Shoe.objects.filter(bin=bin_vo_id)
        else:
            shoe = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoe},
            encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            bin_href = f"/api/bins/{bin_vo_id}/"
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
            {"message": "Invalid bin id"},
            status=400,
        )
    shoe = Shoe.objects.create(**content)
    return JsonResponse(
        shoe,
        encoder=ShoeDetailEncoder,
        safe=False,
    )

@require_http_methods(["DELETE", "GET"])
def api_detail_shoes(request, pk):
    if request.method =="GET":
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            {"shoe":shoe},
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    else:
        count,_ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
