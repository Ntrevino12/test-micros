import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeList from './shoeList';
import ShoeForm from './shoeForm';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes" element={<ShoeList shoes={props.shoes} />} />
          <Route path="shoe/new" element={<ShoeForm/>}/>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
