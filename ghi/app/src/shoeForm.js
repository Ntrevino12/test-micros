import React, { useEffect, useState } from 'react';
function ShoeForm() {
    const [bins, setBins] = useState([]);
    const[bin, setBin] = useState('')
    const [shoename, setName] = useState('');
    const [manufacturer ,setManufacture] = useState('');
    const [color, setColor] = useState('');
    const [picture, setPicture] = useState('');
    const handleNameChange = (event) => {
        setName(event.target.value);
    }
    const handleBinChange = (event) => {
        setBin(event.target.value);
    }
    const handleManufactureChange = (event) => {
        setManufacture(event.target.value);
    }
    const handleColorChange = (event) => {
        setColor(event.target.value);
    }
    const handlePictureChange = (event) => {
        setPicture(event.target.value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = shoename;
        data.manufacturer = manufacturer;
        data.color = color;
        data.picture = picture;
        data.bin = bin;
        const shoeUrl = `http://localhost:8080${bin}shoes/`;
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log("yes",newShoe);
            setName('');
            setBin('');
            setManufacture('');
            setColor('');
            setPicture('');
        }
    }
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        }
    }
    useEffect(() => {
        fetchData();
    }, [])

    function Redirect() {
        window.location.href = 'http://localhost:3000/shoes'
    }

    return (
        <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new shoe</h1>
              <form onSubmit={handleSubmit} id="create-shoe-form">
                <div className="form-floating mb-3">
                    <input onChange={handleManufactureChange} value={manufacturer} placeholder="Manufacturer" type="text" name="manufacturer" id="name" className="form-control" />
                    <label forhtml="manufacturer">Manufacturer</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleNameChange} value={shoename} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                    <label forhtml="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleColorChange} value={color} placeholder="Color" type="text" name="color" id="name" className="form-control" />
                    <label forhtml="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handlePictureChange} value={picture} placeholder="picture" type="url" name="picture" id="name" className="form-control" />
                    <label forhtml="picture">PictureUrl</label>
                </div>
                <div className="mb-3">
                    <select onChange={handleBinChange} value={bin} required id="bin" name="bin" className="form-select">
                        <option value="">Choose a bin</option>
                        {bins.map(bin => {
                        return (
                            <option key={bin.href} value={bin.href}>
                                {bin.closet_name}
                            </option>
                        )
                        })}
                    </select>
                </div>
                <button onClick={Redirect} className="btn btn-primary">Create</button>
                </form>
            </div>
          </div>
        </div>
      </div>
    )
}
export default ShoeForm
