from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Hat, LocationVO
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

# Create your views here.
class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        'closet_name',
        'import_href',
    ]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        'style_name',
    ]


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        'fabric',
        'style_name',
        'color',
        'picture_url',
        'location'
    ]
    encoders = {
        'location': LocationVOEncoder(),
    }



@require_http_methods(['GET', 'POST'])
def api_list_hats(request, location_vo_id=None):
    if request.method == 'GET':
        if location_vo_id is not None:
            hat = Hat.objects.filter(location=location_vo_id)
        else:
            hat = Hat.objects.all()
        return JsonResponse(
            {'hats': hat},
            encoder=HatListEncoder,)
    else:
        content = json.loads(request.body)
        print(content)

        try:
            print('hit------------------------------------------------------------------')
            location_href = f"/api/locations/{location_vo_id}/"
            print("this is loc_href------------", location_href)
            location = LocationVO.objects.get(import_href=location_href)
            print("this is location---------", location)
            content['location'] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )

@require_http_methods(['DELETE', 'GET'])
def api_show_hat(request, pk):
    if request.method == 'GET':
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            {'hat': hat},
            encoder=HatDetailEncoder,
            safe=False,
        )
    else:
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({'deleted': count > 0})
